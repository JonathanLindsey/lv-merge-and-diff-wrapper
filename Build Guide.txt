The difference between the Full Installer and the Update Installer is that the full installer contains the LabVIEW run-time within the installer and the update installer does not.

1. Update the version numbers in all the Build Specifications.

2. Build both of the executables.
3. Build the Installer you want.
OR
2. Right click Build Specifications and select Build All to build both executables and both installers.

4. Go inside the Volume folder of either the full or update installer and open setup.ini.
5. (Optional) Under the [Dialogs] section, find WinFastStartup=1. Change the '1' to '0'. This makes the installer not ask (and not change the setting) to disable Windows fast startup in Windows 8 and 10.

Note: The following 2 steps compress the Volume folder into a single file installer.

6. With 7-zip installed (http://www.7-zip.org/) compress the Volume folder into a Volume.7z archive so the Volume.7z file is in the same folder as the Volume folder.
7. Run the 'Make Installer.bat' file.
8. Repeat steps 4 through 7 for the other full/update installer.