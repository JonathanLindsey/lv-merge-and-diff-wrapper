# Download and install LV Merge and Diff Wrapper

Go to the [downloads page](https://bitbucket.org/JonathanLindsey/lv-merge-and-diff-wrapper/downloads/) to download and run an installer.
The 'full' installers contain the LabVIEW run-time whereas the 'update' installers do not and are much smaller downloads. The first part of the version number refers to the run-time used.

Note: If you are in FRC, LabVIEW for FRC 2020 uses LabVIEW 2019. This is because NI releases new LabVIEW versions in the middle of the year.

# Configure Git Command Line (and thereby other Git GUI Tools) to use LV Merge and Diff Wrapper

Add this to your .gitconfig file usually located at `C:\Users\<Your Username>\.gitconfig`. Make sure you're not making conflicts with other configuration.

```
[diff]
	tool = LV
[merge]
	tool = LV
[difftool "LV"]
	cmd = \"C:/Program Files (x86)/LV Merge and Diff Wrapper/LV Diff Wrapper.exe\" \"$LOCAL\" \"$REMOTE\" -nobdcosm -nobdpos
[mergetool "LV"]
	cmd = \"C:/Program Files (x86)/LV Merge and Diff Wrapper/LV Merge Wrapper.exe\" \"$BASE\" \"$REMOTE\" \"$LOCAL\" \"$MERGED\" -nobdcosm -nobdpos
```

To use LabVIEW's diff tool run `git difftool <......>` NOT `git diff <......>`. For example if test.vi has changes in the working directory and you want to diff the latest changes against the staged version, run `git difftool test.vi`.

To use LabVIEW's merge tool, start the merge with `git merge <branch_name>`. If you have a merge conflict, run `git mergetool <......>` NOT `git merge <......>`. For example if test.vi has a merge conflict, run `git mergetool test.vi`.

##### Notes:
1. If you don't want LV Merge and Diff Wrapper to be your default merge and diff tool, leave out the first 4 lines and add `-t LV` to your `git difftool <......>` command when you want to use LV Merge and Diff Wrapper. If you want to use a name other than LV, change the 4 instances of it in the .gitconfig file.
2. You may change the flags at the end of the cmd strings to fit your preferences. Run `"C:\Program Files (x86)\National Instruments\Shared\LabVIEW Compare\LVCompare.exe"` to get documentation on the available flags. The meaning of the flags in the above configuration are:
  * [-nobdcosm] - Do not compare Block Diagram objects' cosmetics
  * [-nobdpos] - Do not compare Block Diagram objects' position/size
1. On 32-bit computers you must remove the "\[space\](x86)" from the paths.


# Recommended LabVIEW Setting Change
By default LabVIEW compiles code and saves it as part of your VI files. This is problematic when using source control programs like git because it makes it look to git like the VI has been changed when it has only been recompiled. This is one reason why you may often see save dialogs when you have only opened and closed a VI. Configure LabVIEW to not save compiled code in your new VIs:

1. Open LabVIEW
2. Go to Tools -> Options -> Environment -> General
3. Check the "Separate compiled code from new files" checkbox

Configure your existing projects similarly:

1. In your LabVIEW project right click the project item and select properties
2. Check the "Separate compiled code from new project items" checkbox
3. Click the "Mark Existing Items..." button
4. With all the VIs listed selected, click the "Mark Selected Items" button
5. Save all

In the future, when you bring in a VI from an external source, you may need to go to File -> VI Properties -> General and check the "Separate compiled code from source file" checkbox. Sometimes, this setting gets turned off mysteriously and you may need to repeat the above steps for your project.
